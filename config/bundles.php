<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    STS\CalculatorBundle\CalculatorBundle::class => ['all' => true],
];
