<?php
namespace App\Controller;


use STS\CalculatorBundle\Calculator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/api/v1/calculate", "app_calculator_calculate")
     * @return Response
     */
    public function calculate(Request $request, Calculator $calculator)
    {
        $q = $request->query->get('q');
        $result = $calculator->calculate($q);
        return new JsonResponse(["result" => $result]);
    }
}